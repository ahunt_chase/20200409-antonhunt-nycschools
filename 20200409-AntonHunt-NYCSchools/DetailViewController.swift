//
// DetailViewController.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/12/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    @IBOutlet weak var loadingDataView: UIView!
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var nameIndicator: UILabel!
    @IBOutlet weak var readingIndicator: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var mathIndicator: UILabel!
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var writingIndicator: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    @IBOutlet weak var testTakersIndicator: UILabel!
    @IBOutlet weak var numTestTakersLabel: UILabel!
    
    private let viewModel: DetailViewable
    
    init(viewModel: DetailViewable) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func fetchData() {
        viewModel.fetchSATData { [weak self] (error: DetailDataError?) in
            DispatchQueue.main.async {
                if let error = error {
                    let errorMessage = NSLocalizedString("retry", comment: "")
                    let alertMessage: String
                    switch error {
                    case let .badStatus(statusCode: code):
                        alertMessage = NSLocalizedString("statusCallErrorTitle", comment: "") + "\(code)"
                    case let .fetchError(error: error):
                        alertMessage = NSLocalizedString("networkErrorTitle", comment: "")
                        print("Detail Feature Error: \(error.localizedDescription)")
                    case let .jsonError(error: error):
                        alertMessage = NSLocalizedString("jsonErrorTitle", comment: "")
                        print("Detail Feature Error: \(error.localizedDescription)")
                    }
                    self?.showAlert(title: alertMessage, message: errorMessage, handler: { _ in
                        self?.viewModel.resetData()
                        self?.fetchData()
                    })
                } else {
                    self?.setLabelText(data: self?.viewModel.satData)
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameIndicator.text = NSLocalizedString("detailSchoolName", comment: "")
        readingIndicator.text = NSLocalizedString("readingScore", comment: "")
        mathIndicator.text = NSLocalizedString("mathScore", comment: "")
        writingIndicator.text = NSLocalizedString("writingScore", comment: "")
        testTakersIndicator.text = NSLocalizedString("numberOfTestTakers", comment: "")
        
        fetchData()
        loadingDataView.isHidden = true
    }
    
    private func showAlert(title: String, message: String, handler: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okTitle = NSLocalizedString("alertOK", comment: "")
        let cancelTitle = NSLocalizedString("alertCancel", comment: "")
        let okAction = UIAlertAction(title: okTitle, style: .default, handler: handler)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        [okAction, cancelAction].forEach { alert.addAction($0) }
        present(alert, animated: true)
    }
    
    private func setLabelText(data: SATScoreData?) {
        let dataNotAvailable = NSLocalizedString("dataNotAvailable", comment: "")
        schoolNameLabel.text = data?.schoolName ?? dataNotAvailable
        readingScoreLabel.text = data?.criticalReadingAvgScore ?? dataNotAvailable
        mathScoreLabel.text = data?.mathAvgScore ?? dataNotAvailable
        writingScoreLabel.text = data?.writingAvgScore ?? dataNotAvailable
        numTestTakersLabel.text = data?.writingAvgScore ?? dataNotAvailable
    }

}
