//
// CoordinatorFactory.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/12/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

protocol CoordinatoryFactoring {
    func makeDetailCoordintor(dbn: String,  schoolName: String, router: Routable) -> Coordinator
    func makeRootCoordinator() -> Coordinator
}

class CoordinatorFactory: CoordinatoryFactoring {
    func makeDetailCoordintor(dbn: String, schoolName: String, router: Routable) -> Coordinator {
        return DetailsCoordinator(dbn: dbn, schoolName: schoolName, router: router)
    }
    
    func makeRootCoordinator() -> Coordinator {
        return RootCoordinator()
    }
}
