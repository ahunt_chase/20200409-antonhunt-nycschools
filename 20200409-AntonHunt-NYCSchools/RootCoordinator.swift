//
// RootCoordinator.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/10/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

import UIKit

protocol RootNavDelegate: AnyObject {
    func cellTapped(dbn: String, schoolName: String)
}

protocol CoordinatorType: Hashable {}

enum RootCoordinatorType {
    case root
    case details
}

protocol Coordinator: AnyObject {
    func start()
    func childCoordinators() -> [Coordinator]
}

class RootCoordinator: Coordinator {
    func childCoordinators() -> [Coordinator] {
        return siblingCoordinators
    }
    
    private let router: Routable
    private let siblingCoordinators: [Coordinator]
    private let coordinatorFactory: CoordinatoryFactoring
    
    init(router: Routable = Router(),
         siblingCoordinators: [Coordinator] = [],
         coordinatorFactory: CoordinatoryFactoring = CoordinatorFactory()) {
        self.router = router
        self.siblingCoordinators = siblingCoordinators
        self.coordinatorFactory = coordinatorFactory
    }
    
    public func start() {
        router.navController.viewControllers = [ RootViewController(delegate: self)]
        let window = UIApplication.shared.windows.first
        window?.rootViewController = router.navController
        window?.makeKeyAndVisible()
    }
}

extension RootCoordinator: RootNavDelegate {
    func cellTapped(dbn: String, schoolName: String) {
        coordinatorFactory.makeDetailCoordintor(dbn: dbn, schoolName: schoolName, router: router).start()
    }
}
