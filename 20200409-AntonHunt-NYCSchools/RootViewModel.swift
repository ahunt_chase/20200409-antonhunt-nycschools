//
// RootViewModel.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/11/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

protocol RootViewable {
    func fetchSchoolData(dataFetchHandler: @escaping (_ result: Result<Range<Int>, RootDataError>) -> Void)
    var schools: [SchoolData] { get }
    var delegate: RootNavDelegate? { get set }
    func resetData()
}

class RootViewModel: RootViewable {
    private var schoolData = [SchoolData]()
    private let rootDataController: RootDataControlling?
    private let dataLimitPerFetch: Int
    var delegate: RootNavDelegate?
    
    init(rootDataController: RootDataControlling?,
         dataLimitPerFetch: Int) {
        self.rootDataController = rootDataController
        self.dataLimitPerFetch = dataLimitPerFetch
    }
    
    convenience init() {
        self.init(rootDataController: RootDataController(),
                  dataLimitPerFetch: 20)
    }
    
    var schools: [SchoolData] {
        return schoolData
    }
    
    func resetData() {
        schoolData = []
    }
    
    func fetchSchoolData(dataFetchHandler: @escaping (_ result: Result<Range<Int>, RootDataError>) -> Void) {
        rootDataController?.fetchSchoolData(offset: schoolData.count, limit: dataLimitPerFetch) { [weak self] (result: Result<[SchoolData], RootDataError>) in
            switch result {
            case .success(let schoolData):
                if let self = self {
                    let rangeToReload = self.schoolData.count ..< self.schoolData.count+self.dataLimitPerFetch
                    dataFetchHandler(.success(rangeToReload))
                    self.schoolData.append(contentsOf: schoolData)
                }
            case .failure(let rootDataError):
                dataFetchHandler(.failure(rootDataError))
            }
        }
    }
}
