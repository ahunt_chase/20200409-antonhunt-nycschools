//
// TableViewCellIdentifiable.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/11/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

import UIKit

public protocol TableViewCellIdentifiable where Self:UITableViewCell {
    static var identifier: String { get }
}

extension TableViewCellIdentifiable {
    static var identifier: String {
        return "\(Self.self)"
    }
}
