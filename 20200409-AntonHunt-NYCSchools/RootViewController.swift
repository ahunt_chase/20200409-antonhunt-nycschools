//
// RootViewController.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/11/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    private var rootViewModel: RootViewable
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    init(rootViewModel: RootViewable = RootViewModel(),
         delegate: RootNavDelegate) {
        self.rootViewModel = rootViewModel
        self.rootViewModel.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        tableView.register(UINib(nibName: RootTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: RootTableViewCell.identifier)
        tableView.separatorInsetReference = .fromCellEdges
        tableView.dataSource = self
        tableView.delegate = self
        tableView.prefetchDataSource = self
        
        navigationController?.navigationBar.prefersLargeTitles = true
        title = NSLocalizedString("rootViewControllerTitle", comment: "")
        fetchData()
    }

    private func showAlert(title: String, message: String, handler: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okTitle = NSLocalizedString("alertOK", comment: "")
        let cancelTitle = NSLocalizedString("alertCancel", comment: "")
        let okAction = UIAlertAction(title: okTitle, style: .default, handler: handler)
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        [okAction, cancelAction].forEach { alert.addAction($0) }
        present(alert, animated: true)
    }
    
    private func fetchData() {
        rootViewModel.fetchSchoolData { [weak self] (result: Result<Range<Int>, RootDataError>) in
            DispatchQueue.main.async {
                switch result {
                case let .success(pathsToReload):
                    let indexesToReload = pathsToReload.map { IndexPath(row: $0, section: 0) }
                    self?.tableView.insertRows(at: indexesToReload, with: .automatic)
                case let .failure(rootDataError):
                    let errorMessage = NSLocalizedString("retry", comment: "")
                    let alertMessageTitle: String
                    switch rootDataError {
                    case let .badStatus(statusCode: code):
                        alertMessageTitle = NSLocalizedString("statusCallErrorTitle", comment: "") + "\(code)"
                    case let .fetchError(error: error):
                        alertMessageTitle = NSLocalizedString("networkErrorTitle", comment: "")
                        print("Root Feature Error: \(error.localizedDescription)")
                    case let .jsonError(error: error):
                        alertMessageTitle = NSLocalizedString("jsonErrorTitle", comment: "")
                        print("Root Feature Error: \(error.localizedDescription)")
                    }
                    self?.showAlert(title: alertMessageTitle, message: errorMessage) { _ in
                        self?.rootViewModel.resetData()
                        self?.fetchData()
                    }
                }
            }
        }
    }
}

extension RootViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rootViewModel.schools.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: RootTableViewCell.identifier, for: indexPath)
        if let rootCell = cell as? RootTableViewCell {
            rootCell.setupLayout(data: rootViewModel.schools[indexPath.row])
            return rootCell
        }
        return cell
    }
}

extension RootViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let schoolData = rootViewModel.schools[indexPath.row]
        if let dbn = schoolData.dbn, let name = schoolData.schoolName {
            rootViewModel.delegate?.cellTapped(dbn: dbn, schoolName: name)
        }
    }
}

extension RootViewController: UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        if indexPaths.contains(where: { $0.row >= rootViewModel.schools.count-1 }) {
            fetchData()
        }
    }
}
