//
// SchoolData.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/11/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

struct SchoolData: Codable {
    
    let dbn: String?
    let schoolName: String?
    let location: String?
    let number: String?
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case location
        case number = "phone_number"
    }
    
}
