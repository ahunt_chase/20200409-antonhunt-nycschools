//
// Router.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/10/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

import UIKit

public protocol Routable {
    func push(vc: UIViewController, animated: Bool, completionHandler: (()-> Void)?)
    func pop(vc: UIViewController, animated: Bool, completionHandler: (()-> Void)?)
    func present(vc: UIViewController, animated: Bool, completionHandler: (()-> Void)?)
    func dismiss( animated: Bool, completionHandler: (()-> Void)?)
    var navController: UINavigationController { get }
}

public class Router: Routable {
    private let navigationController: UINavigationController
    
    public init(navigationController: UINavigationController = UINavigationController()) {
        self.navigationController = navigationController
    }
    
    public var navController: UINavigationController {
        return navigationController
    }
    
    public func push(vc: UIViewController, animated: Bool, completionHandler: (()-> Void)? = nil) {
        navigationController.pushViewController(vc, animated: animated)
        completionHandler?()
    }
    
    public func pop(vc: UIViewController, animated: Bool, completionHandler: (()-> Void)? = nil) {
        navigationController.popViewController(animated: animated)
        completionHandler?()
    }
    
    public func present(vc: UIViewController, animated: Bool, completionHandler: (()-> Void)? = nil) {
        navigationController.present(vc, animated: animated, completion: completionHandler)
    }
    
    public func dismiss( animated: Bool, completionHandler: (()-> Void)? = nil) {
        navigationController.dismiss(animated: animated, completion: completionHandler)
    }
}
