//
// RootDataController.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/11/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

import Foundation

protocol RootDataControlling {
    func fetchSchoolData(offset: Int, limit: Int, handler: @escaping (Result<[SchoolData], RootDataError>)->Void)
}

enum RootDataError: Error {
    case badStatus(statusCode: Int)
    case fetchError(error: Error)
    case jsonError(error: Error)
}

class RootDataController: RootDataControlling {
    
    private let baseURL: URL
    private let config: URLSessionConfiguration
    private let session: URLSession
    
    init(baseURL: URL,
         config: URLSessionConfiguration) {
        self.baseURL = baseURL
        self.config = config
        self.session = URLSession(configuration: config)
    }
    
    convenience init?() {
        let textUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        guard let url = URL(string: textUrl) else { return nil  }
        self.init(baseURL: url, config: URLSessionConfiguration.default)
    }
    
    private func queryURL(offset: Int, limit: Int, orderBy: String = SchoolData.CodingKeys.dbn.rawValue) -> URL? {
        let limitQuery = URLQueryItem(name: "$limit", value: "\(limit)")
        let offsetQuery = URLQueryItem(name: "$offset", value: "\(offset)")
        let orderByQuery = URLQueryItem(name: "$order", value: "\(orderBy) ASC")
        var urlComponent = URLComponents(string: baseURL.absoluteString)
        urlComponent?.queryItems = [offsetQuery, limitQuery, orderByQuery]
        return urlComponent?.url
    }
    
    func fetchSchoolData(offset: Int, limit: Int, handler: @escaping (Result<[SchoolData], RootDataError>)->Void) {
        if let dataURL = queryURL(offset: offset, limit: limit) {
            session.dataTask(with: dataURL) { (data: Data?, response: URLResponse?, error: Error?) in
                if let data = data, let response = response as? HTTPURLResponse {
                    if (200...299).contains( response.statusCode) {
                        do {
                            let schoolData = try JSONDecoder().decode([SchoolData].self, from: data)
                            handler(.success(schoolData))
                        } catch  {
                            handler(.failure(.jsonError(error: error)))
                        }
                    } else {
                        handler(.failure(.badStatus(statusCode: response.statusCode)))
                    }
                } else if let error = error {
                    handler(.failure(.fetchError(error: error)))
                }
            }.resume()
        }
    }
}
