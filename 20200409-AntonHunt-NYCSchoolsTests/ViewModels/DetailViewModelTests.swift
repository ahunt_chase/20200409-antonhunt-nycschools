//
// DetailViewModelTests.swift
// 20200409-AntonHunt-NYCSchoolsTests
//
// Created by My Name and Ohter Name on 4/13/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

import XCTest
@testable import AntonHuntNYCSchools

class DetailViewModelTests: XCTestCase {
    
    lazy var subject = DetailViewModel(detailDataController: nil, dbn: "", schoolName: "")

    func testResetData() {
        // TODO: - Bad Test since satData starts as nil.  Make it useful.
        subject.resetData()
        XCTAssertNil(subject.satData)
    }

}
