//
// RootTableViewCell.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/11/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

import UIKit

class RootTableViewCell: UITableViewCell, TableViewCellIdentifiable {

    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 3, left: 2, bottom: 3, right: 2))
    }
    
    func setupLayout(data: SchoolData) {
        schoolNameLabel.text = data.schoolName
        let location: Substring? = data.location?.prefix(while: { $0 != "("})
        locationLabel.text = location.map(String.init)
        phoneNumberLabel.text = data.number
    }
    
}
