//
// DetailsCoordinator.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/12/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

class DetailsCoordinator: Coordinator {
    func childCoordinators() -> [Coordinator] {
        return siblingCoordinators
    }
    
    private let router: Routable
    private let siblingCoordinators: [Coordinator]
    private let dbn: String
    private let schoolName: String
    
    init(dbn: String,
         schoolName: String,
         router: Routable = Router(),
         siblingCoordinators: [Coordinator] = []) {
        self.router = router
        self.siblingCoordinators = siblingCoordinators
        self.dbn = dbn
        self.schoolName = schoolName
    }
    
    func start() {
        let viewModel = DetailViewModel(dbn: dbn, schoolName: schoolName)
        let detailVC = DetailViewController(viewModel: viewModel)
        router.push(vc: detailVC, animated: true, completionHandler: nil)
    }
    
    
    
}
