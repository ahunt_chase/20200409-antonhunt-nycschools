//
// DetailViewModel.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/12/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

protocol DetailViewable {
    var satData: SATScoreData? { get }
    func fetchSATData(dataFetchHandler: @escaping (_ error: DetailDataError?) -> Void)
    func resetData()
}

class DetailViewModel: DetailViewable {
    
    private var data: SATScoreData?
    private let detailDataController: DetailDataControlling?
    private let dbn: String
    private let schoolName: String
    
    init(detailDataController: DetailDataControlling?,
         dbn: String,
         schoolName: String) {
        self.detailDataController = detailDataController
        self.dbn = dbn
        self.schoolName = schoolName
    }
    
    convenience init(dbn: String, schoolName: String) {
        self.init(detailDataController: DetailDataController(),
                  dbn: dbn,
                  schoolName: schoolName)
    }
    
    var satData: SATScoreData? {
        return data
    }
    
    func fetchSATData(dataFetchHandler: @escaping (_ error: DetailDataError?) -> Void) {
        detailDataController?.fetchSATData(dbn: dbn, handler: { [weak self] (result: Result<[SATScoreData], DetailDataError>) in
            switch result {
            case let .success(satData):
                let unavailableSchoolData = SATScoreData(dbn: self?.dbn, schoolName: self?.schoolName, numSatTestTakers: nil, criticalReadingAvgScore: nil, mathAvgScore: nil, writingAvgScore: nil)
                self?.data = satData.first ?? unavailableSchoolData
                dataFetchHandler(nil)
            case let .failure(detailError):
                dataFetchHandler(detailError)
            }
        })
    }
    
    func resetData() {
        data = nil
    }
}
