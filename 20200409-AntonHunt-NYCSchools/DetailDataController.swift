//
// DetailDataController.swift
// 20200409-AntonHunt-NYCSchools
//
// Created by Anton Hunt on 4/12/20.
// Copyright © 2020 Your Company Name. All rights reserved.
//

import Foundation

protocol DetailDataControlling {
    func fetchSATData(dbn: String, handler: @escaping (Result<[SATScoreData], DetailDataError>)->Void)
}

enum DetailDataError: Error {
    case badStatus(statusCode: Int)
    case fetchError(error: Error)
    case jsonError(error: Error)
}

class DetailDataController: DetailDataControlling {
    
    private let baseURL: URL
    private let config: URLSessionConfiguration
    private let session: URLSession
    
    init(baseURL: URL, config: URLSessionConfiguration) {
        self.baseURL = baseURL
        self.config = config
        self.session = URLSession(configuration: config)
    }
    
    convenience init?() {
        let textUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        guard let url = URL(string: textUrl) else { return nil  }
        self.init(baseURL: url, config: URLSessionConfiguration.default)
    }
    
    func fetchSATData(dbn: String, handler: @escaping (Result<[SATScoreData], DetailDataError>) -> Void) {
        if let dataURL = queryURL(dbn: dbn) {
            session.dataTask(with: dataURL) { (data: Data?, response: URLResponse?, error: Error?) in
                if let data = data, let response = response as? HTTPURLResponse {
                    if (200...299).contains( response.statusCode) {
                        do {
                            let satData = try JSONDecoder().decode([SATScoreData].self, from: data)
                            handler(.success(satData))
                        } catch  {
                            handler(.failure(.jsonError(error: error)))
                        }
                    } else {
                        handler(.failure(.badStatus(statusCode: response.statusCode)))
                    }
                } else if let error = error {
                    handler(.failure(.fetchError(error: error)))
                }
            }.resume()
        }
    }
    
    private func queryURL(dbn: String) -> URL? {
        let dbnQuery = URLQueryItem(name: "dbn", value: dbn)
        var urlComponent = URLComponents(string: baseURL.absoluteString)
        urlComponent?.queryItems = [dbnQuery]
        return urlComponent?.url
    }
}
